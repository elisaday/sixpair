# SixPair


### 1. Description
Sixaxis/DualShock3 is a wireless controller produced by Sony since 2006 for their PlayStation 3. Sixaxis controller uses Bluetooth to connect to the host master (PS3, computer, smartphone, ...) but pairing is done with a physical USB connection.

SixPair allow USB pairing of a Sixaxis controller with a non-PS3 Bluetooth host master.


### 2. Usage

#### Requirements
+ Your Linux kernel version must be at least 2.6.21 :

```
#!bash
$ uname -a
```

+ Install packages `gcc`, `libusb-dev` and `libusb-0.1-4` :

```
#!bash
$ sudo apt-get install gcc libusb-dev libusb-0.1-4
```

#### build
```
#!bash
$ gcc -o sixpair sixpair.c -lusb
```

#### set Bluetooth master address
Default master is the computer used to run sixpair :

```
#!bash
$ sudo ./sixpair
```

Set a device with Bluetooth address "xx:xx:xx:xx:xx:xx" as a specific host master :

```
#!bash
$ sudo ./sixpair "xx:xx:xx:xx:xx:xx"
```


### 3. Origin
Original "sixaxis.c" (from 2007-04-18) can be found in the following pages :

+ <http://www.pabr.org/sixlinux/sixlinux.en.html>

+ <https://help.ubuntu.com/community/Sixaxis>


### 4. Licence
+ Original "sixaxis.c" source file don't have explicit licence and author.
+ "Sixaxis" is a trademark of Sony Computer Entertainment Inc.


### 5. Other links
+ Sixaxis for your RaspberryPi : <http://booting-rpi.blogspot.ro/>
+ Graphical interface to connect your Sixaxis controller : <http://qtsixa.sourceforge.net/>
+ Sixaxis for your Android device : <http://www.dancingpixelstudios.com/sixaxiscontroller/instructions.html>
